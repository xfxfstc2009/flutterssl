import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

class HttpManager {
  static Future getHttp() async {
    BaseOptions options = BaseOptions(
        method: "get",
        baseUrl: "https://system-api-test.sharechef.com/",
        queryParameters: {
          "sys_token":
              "Sys-eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiIxNyIsInN1YiI6IjE4MDQyMzE5MzAwIiwiaWF0IjoxNjA0MjgwNjY4LCJpc3MiOiJjb20vc2hhcmVjaGVmIiwiZXhwIjoxNjA2ODcyNjY4fQ.SEwoSVlrOKq4e-sMXxbXlH8g7m-zO5FasQbl0HEls9ZV8psKLQtZWfO4y_VomxNAdEXDtNEYdrz5Uncp6BzbrQ",
        },
        headers: {
          HttpHeaders.acceptHeader: "*",
          "sys_token":
              "Sys-eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiIxNyIsInN1YiI6IjE4MDQyMzE5MzAwIiwiaWF0IjoxNjA0MjgwNjY4LCJpc3MiOiJjb20vc2hhcmVjaGVmIiwiZXhwIjoxNjA2ODcyNjY4fQ.SEwoSVlrOKq4e-sMXxbXlH8g7m-zO5FasQbl0HEls9ZV8psKLQtZWfO4y_VomxNAdEXDtNEYdrz5Uncp6BzbrQ",
        });
    Dio netRequest = Dio(options);
    final response = await netRequest.get('/ofc/packer/byId/5');
    return response;
  }
}

class smartModel {
  var id;
  var storeId;
  var store;
}

class HttpRequest {
  //1. 创建dio实例
  static BaseOptions baseOptions = BaseOptions(
    baseUrl: "",
    connectTimeout: 10,
    contentType: "application/json; charset=utf-8", // 设置content-type
    headers: {
      "packer_token":
          "Packer-eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI1Iiwic3ViIjoiMzMxMjMxIiwiaWF0IjoxNjA0MzA5NDMwLCJpc3MiOiJjb20vc2hhcmVjaGVmIiwiZXhwIjoxNjA2OTAxNDMwfQ.ZlWGmoQy867EWlrba7lzfQY3kLziLVFr_hAn1Xh9N6bAJtGUB6jZbXdDwjdGYGqXnoskUGUtrwOOQmV8QkHBQA"
    },
    responseType: ResponseType.json,
  );
  static final dio = Dio(baseOptions);

  ///get请求
  static Future request(String url,
      {String method = "get", Map<String, dynamic> param}) async {
    //发送请求
    Options options = Options(method: method);

    final result =
        await dio.request(url, queryParameters: param, options: options);
    return result;
  }

  ///post请求
  static Future postRequest(String url,
      {String method = "post", Map<String, dynamic> param}) async {
    //发送请求
    Options options = Options(method: method);

    print('请求${url}参数打印==== ${param}');

    try {
      final result =
          await dio.request(url, queryParameters: param, options: options);
      final responseData = result.data;
      print(result.data);
      if (responseData['code'].toString() == '10000') {
        return responseData['data'];
      } else {
        return responseData['data'];
      }
    } on DioError catch (err) {
      throw err;
    }
  }
}

List listData = [
  {
    "title": "卡兹脆鸡饭",
    "des": "咔滋脆咔滋脆，一口下去好吃美...",
    "imageUrl": "https://www.itying.com/images/flutter/1.png"
  },
  {
    "title": "卡兹脆鸡饭",
    "des": "咔滋脆咔滋脆，一口下去好吃美...",
    "imageUrl": "https://www.itying.com/images/flutter/2.png"
  },
  {
    "title": "卡兹脆鸡饭",
    "des": "咔滋脆咔滋脆，一口下去好吃美...",
    "imageUrl": "https://www.itying.com/images/flutter/3.png"
  },
  {
    "title": "卡兹脆鸡饭",
    "des": "咔滋脆咔滋脆，一口下去好吃美...",
    "imageUrl": "https://www.itying.com/images/flutter/4.png"
  },
  {
    "title": "卡兹脆鸡饭",
    "des": "咔滋脆咔滋脆，一口下去好吃美...",
    "imageUrl": "https://www.itying.com/images/flutter/5.png"
  }
];
