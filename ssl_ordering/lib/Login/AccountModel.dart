class AccountModel {
  // 工厂模式
  factory AccountModel() => _getInstance();
  static AccountModel get instance => _getInstance();
  static AccountModel _instance;
  AccountModel._internal() {
    // 初始化
  }
  static AccountModel _getInstance() {
    if (_instance == null) {
      _instance = new AccountModel._internal();
    }
    return _instance;
  }
}
