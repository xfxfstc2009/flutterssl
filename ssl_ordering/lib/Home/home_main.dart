import 'package:flutter/material.dart';
import 'package:ssl_ordering/Tool/IconTextButton.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class HomeMainViewController extends StatefulWidget {
  HomeMainViewController({Key key}) : super(key: key);

  @override
  _HomeMainViewControllerState createState() => _HomeMainViewControllerState();
}

class _HomeMainViewControllerState extends State<HomeMainViewController> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            image: DecorationImage(
          image: NetworkImage(
              'https://img.zcool.cn/community/0372d195ac1cd55a8012062e3b16810.jpg'),
          fit: BoxFit.cover,
        )),
        child: Scaffold(
            backgroundColor: Colors.transparent, //把scaffold的背景色改成透明
            appBar: PreferredSize(
              child: AppBar(
                backgroundColor: Colors.transparent, //把appbar的背景色改成透明
                leading: _appBarPopMenuButton(),
                elevation: 3, //appbar的阴影
                title: Text(
                  "工作间",
                  style: TextStyle(color: Color(0xFFFFFFFF)),
                ),
                actions: [
                  IconTextButton.icon(
                    icon: Icon(
                      Icons.account_circle,
                      size: 30,
                    ),
                    label: Text("1231234334"),
                    color: Colors.transparent,
                    textColor: Colors.white,
                    elevation: 4.0,
                    iconTextAlignment: IconTextAlignment.iconLeftTextRight,
                    onPressed: () {},
                  ),
                  IconButton(
                    icon: Icon(Icons.settings),
                    iconSize: 50,
                    onPressed: () {},
                    color: Colors.white,
                    disabledColor: Colors.black,
                    splashColor: Colors.yellow,
                    highlightColor: Colors.orange,
                  ),
                  IconButton(
                      icon: Icon(Icons.power_settings_new),
                      iconSize: 50,
                      onPressed: () {},
                      color: Colors.white,
                      disabledColor: Colors.black,
                      splashColor: Colors.yellow,
                      highlightColor: Colors.orange)
                ],
              ),
              preferredSize: Size.fromHeight(80.0),
            ),
            body: WillPopScope(
              child: _infoCollectionView(),
              onWillPop: () async {
                return false;
              },
            )));
  }

  PopupMenuButton _appBarPopMenuButton() {
    return PopupMenuButton(itemBuilder: (BuildContext context) {
      return [
        PopupMenuItem(
          child: Text("DOTA"),
          value: "dota",
        ),
        PopupMenuItem(
          child: Text("英雄联盟"),
          value: ["盖伦", "皇子", "提莫"],
        ),
        PopupMenuItem(
          child: Text("王者荣耀"),
          value: {"name": "王者荣耀"},
        ),
      ];
    }, onSelected: (Object object) {
      print(object);
    }, onCanceled: () {
      print("canceled");
    });
  }

  // top 栏目
  Column _mainView() {
    final size = MediaQuery.of(context).size;
    final width = size.width;
    final height = size.height;

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          height: 60,
          child: Row(
            children: [
              Expanded(child: Container()),
              Expanded(child: Container()),
              Expanded(child: Container()),
            ],
          ),
        ),
        Container(
          child: Text("123"),
        )
      ],
    );
  }

  // 内容瀑布流页面
  StaggeredGridView _infoCollectionView() {
    return StaggeredGridView.countBuilder(
        crossAxisCount: 5, // 列数，一行有几个元素
        mainAxisSpacing: 10, // 主轴方向上的边距
        crossAxisSpacing: 10, // 次轴方向上的边距
        itemCount: 10, // 最多显示10个
        itemBuilder: (BuildContext context, int index) => new Container(
              child: _singleCard('$index'),
            ),
        staggeredTileBuilder: (index) => StaggeredTile.fit(1));
  }

  Card _singleCard(String info) {
    print(info);
    return Card(
      elevation: 4.0, // 阴影
      color: Color(0xffebe6da),
      child: new Container(
        color: Color(0xffebe6da),
        width: 200.0,
        height: 200.0,
        child: _singleCardColum(),
      ),
    );
  }

  ListView _singleCardColum() {
    return ListView(
      padding: EdgeInsets.fromLTRB(12, 15, 12, 15),
      children: [
        // 1. 编号
        Text(
          "#0001",
          textAlign: TextAlign.left,
          style: TextStyle(
            color: Color(0xff534C3C),
            fontSize: 18,
          ),
        ),
        // 2.日期
        Container(
          child: Text(
            "2020/10/10 13:14",
            textAlign: TextAlign.left,
            style: TextStyle(
              color: Color(0xffC0B6A5),
              fontSize: 10,
            ),
          ),
          padding: EdgeInsets.fromLTRB(0, 7, 0, 10),
        ),
        // 3.横线
        Divider(
          color: Color(0xffC0B6A5),
        ),
        // 4.菜单列表
        Container(
          height: 400.0,
          color: Colors.yellow,
          child: _singleCardListView(),
        ),

        // 5.横线
        Divider(
          color: Color(0xffC0B6A5),
        ),
        // 6.合计
        Row(
          children: [
            Expanded(
              flex: 1,
              child: Text(
                "合计",
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: Color(0xff534C3C),
                  fontSize: 10,
                ),
              ),
            ),
            Expanded(
                flex: 1,
                child: Text(
                  "x 6",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    color: Color(0xff534C3C),
                    fontSize: 18,
                  ),
                )),
          ],
        ),
        // 7.按钮
        Container(
            child: FlatButton(
                child: Text(
                  "打印小票",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 13,
                  ),
                ),
                color: Color(0xffB5895C),
                onPressed: () {
                  print("1");
                })),
      ],
    );
  }

  ListView _singleCardListView() {
    return ListView(
      scrollDirection: Axis.horizontal, // 1.设置列表的滚动方向
      // shrinkWrap: true, // 2.该属性将决定列表的长度是否仅包裹其内容的长度。
      padding: EdgeInsets.fromLTRB(25, 0, 25, 0), // 边距
      children: [
        _singleCardCell(),
      ],
    );
  }

  Container _singleCardCell() {
    return Container(
      height: 40,
      width: 100,
      child: Row(
        children: [
          // 1. 创建一个label
          Expanded(flex: 1, child: Text("鱼香肉丝")),
          // 2.数量
          Expanded(flex: 1, child: Text("X1")),
          // 3.图片
          Expanded(
            flex: 1,
            child:
                Image(image: AssetImage('assets/images/login/login_logo.png')),
          ),
        ],
      ),
    );
  }
}
