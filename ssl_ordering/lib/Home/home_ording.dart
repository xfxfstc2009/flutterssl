import 'package:flutter/material.dart';
import 'package:ssl_ordering/Home/home_ording_data.dart';
import 'dart:convert';

class HomeOrdingVC extends StatefulWidget {
  HomeOrdingVC({Key key}) : super(key: key);

  @override
  _HomeOrdingVCState createState() => _HomeOrdingVCState();
}

class _HomeOrdingVCState extends State<HomeOrdingVC> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.lightBlue,
          title: Text("点菜页面"),
          actions: [
            IconButton(
                icon: Icon(Icons.search),
                onPressed: () {
                  _getData();
                })
          ],
        ),
        body: _bodyContainer(),
      ),
    );
  }

  Column _bodyContainer() {
    return Column(
      children: [
        Expanded(
            flex: 1,
            child: Container(
                color: Colors.red,
                child: Row(
                  children: [
                    SizedBox(
                      width: 20,
                    ),
                    Container(
                      child: Image.network(
                          "https://www.itying.com/images/flutter/1.png"),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      child: Text("十四郎滨江店"),
                    )
                  ],
                ))),
        Expanded(
            flex: 20,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(flex: 3, child: leftListView()),
                Expanded(flex: 9, child: rightListView(null)),
              ],
            )),
        Expanded(
            flex: 2,
            child: Container(
              color: Colors.red,
              child: Row(
                children: [
                  Expanded(
                      flex: 2,
                      child: FlatButton(
                        height: 100,
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text("取消",
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                            )),
                      )),
                  Expanded(
                    flex: 1,
                    child: FloatingActionButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      // child: Image.network(
                      //     "http://tiebapic.baidu.com/forum/w%3D580/sign=a96ca741eafaaf5184e381b7bc5594ed/7ea6a61ea8d3fd1f2643ad5d274e251f95ca5f38.jpg"),
                    ),
                  ),
                  Expanded(child: (Container())),
                  Expanded(
                      flex: 2,
                      child: FlatButton(
                        height: 100,
                        onPressed: () {
                          _getData();
                        },
                        child: Text("选好了 >",
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                            )),
                      )),
                ],
              ),
            ))
      ],
    );
  }

  ListView leftListView() {
    return ListView(
      children: <Widget>[
        Text(
          "1",
          style: TextStyle(fontWeight: FontWeight.w800),
        ),
        Text(
          "1",
          style: TextStyle(fontWeight: FontWeight.w800),
        ),
        Text(
          "1",
          style: TextStyle(fontWeight: FontWeight.w800),
        ),
        Text(
          "1",
          style: TextStyle(fontWeight: FontWeight.w800),
        ),
        Text(
          "1",
          style: TextStyle(fontWeight: FontWeight.w800),
        ),
        Text(
          "1",
          style: TextStyle(fontWeight: FontWeight.w800),
        ),
        Text(
          "1",
          style: TextStyle(fontWeight: FontWeight.w800),
        ),
      ],
    );
  }

  List listDatas = [];

  void _getData() {
    HttpManager.getHttp().then((response) {
      Map<String, dynamic> news = jsonDecode(response.toString());
      List tlistDatas = news["data"]["packerExList"];

      listDatas = [
        ...tlistDatas,
        ...tlistDatas,
        ...tlistDatas,
        ...tlistDatas,
        ...tlistDatas,
        ...tlistDatas,
        ...tlistDatas,
        ...tlistDatas,
        ...tlistDatas,
        ...tlistDatas,
        ...tlistDatas,
        ...tlistDatas,
        ...tlistDatas,
        ...tlistDatas
      ];

      print(listDatas);

      setState(() {});
    });
  }

  void _smaShowAlert(info) {
    HttpManager.getHttp().then((response) {
      Map<String, dynamic> news = jsonDecode(response.toString());
      String ins = news["code"].toString();

      _showAlert(info, ins);
    });
  }

  void _showAlert(info, info1) {
    showDialog<Null>(
      context: context,
      builder: (BuildContext context) {
        return new SimpleDialog(
          title: new Text(info),
          children: <Widget>[
            new SimpleDialogOption(
              child: new Text(info1),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new SimpleDialogOption(
              child: new Text('选项 2'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    ).then((val) {
      print(val);
    });
  }

  Widget getRow(int i) {
    if (listDatas != null) {
      var info = listDatas[i]["id"].toString();
      return GestureDetector(
        child: Padding(padding: EdgeInsets.all(10.0), child: Text(info)),
        onTap: () {
          setState(() {
            _smaShowAlert(info);
          });
        },
      );
    } else {
      return GestureDetector(
        child: Padding(padding: EdgeInsets.all(10.0), child: Text("1")),
        onTap: () {
          setState(() {
            _smaShowAlert("123");
          });
        },
      );
    }
  }

  ListView rightListView(transferInfo) {
    return ListView.builder(
      itemCount: listDatas.length,
      itemBuilder: (context, index) {
        return getRow(index);
      },
    );
  }
}
