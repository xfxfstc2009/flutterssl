import 'package:flutter/material.dart';
import 'package:ssl_ordering/Tool/Network/APIConstance.dart';
import 'package:ssl_ordering/Tool/Network/ResultData.dart';
import 'package:ssl_ordering/Tool/RSAManager.dart';
import 'package:ssl_ordering/Tool/Network/NetworkAdapter.dart';
import 'package:ssl_ordering/Tool/Network/DataHelper.dart';

class LoginViewController extends StatefulWidget {
  LoginViewController({Key key}) : super(key: key);

  @override
  _LoginViewControllerState createState() => _LoginViewControllerState();
}

class _LoginViewControllerState extends State<LoginViewController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: Colors.blue,
        appBar: AppBar(
          backgroundColor: Colors.lightBlue,
          title: Text("登录"),
        ),
        body: _mainLoginColum(),
      ),
    );
  }

  // 1.设置一个大框架
  Container _mainLoginColum() {
    return Container(
      child: _loginInfoView(),
      padding: EdgeInsets.symmetric(horizontal: 30),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle, // 设置圆角
          borderRadius: BorderRadius.all(Radius.circular(20)), // 圆角
          color: Colors.white),
      alignment: Alignment(0, 0),
      margin: EdgeInsets.all(40.0),
    );
  }

  // 2.设置一个内容框架
  Column _loginInfoView() {
    return Column(
      children: [
        // logo
        Expanded(child: Container()),

        Expanded(
            child: Image(
          image: new AssetImage('assets/images/login/login_logo.png'),
          width: 200,
          height: 200,
          fit: BoxFit.cover,
        )),
        // 空行
        Expanded(
          child: Container(),
        ),
        // 手机号
        Expanded(
            child: TextField(
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.all(10.0),
            labelText: '请输入手机号码)',
          ),
          maxLength: 11,
          onChanged: _phoneNumberChanged,
          cursorColor: Color.fromARGB(1, 252, 131, 15),
          autofocus: true,
        )),
        // 密码
        Expanded(
            child: TextField(
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.all(10.0),
            labelText: '请输入密码',
          ),
          maxLength: 11,
          onChanged: _phoneNumberChanged,
          cursorColor: Color.fromARGB(1, 252, 131, 15),
          autofocus: true,
        )),
        // 登录按钮
        Expanded(
            flex: 1,
            child: Container(
              child: MaterialButton(
                color: Colors.orange[400],
                textColor: Colors.white,
                child: new Text('立即登录'),
                onPressed: () {
                  // Navigator.pushNamed(context, "homeViewController");
                  _interfaceManager("15669969617", "666666");
                },
                minWidth: 300,
                height: 30,
              ),
              padding: EdgeInsets.fromLTRB(30, 30, 30, 30),
            )),
        // 底层
        Expanded(child: Container())
      ],
    );
  }

  // 手机号输入进行判断
  void _phoneNumberChanged(String info) {
    print(info);
  }

  // 接口进行登录
  void _interfaceManager(String phoneNumber, String pwd) {
    final future = RSAManager.encode("666666");
    future.then((value) => loginManagerMain(phoneNumber, value));
  }

  void loginManagerMain(String phone, String pwd) async {
    var params = DataHelper.getBaseMap();
    params.clear();
    params["phone"] = phone.toString();
    params["password"] = pwd;
    ResultData res = await NetworkAdapter.getInstance()
        .post(APIManager.login_login, params: params);
    setState(() {
      if (res.isSuccess) {
        print("success");
      } else {
        print("err");
      }
    });
  }
}
