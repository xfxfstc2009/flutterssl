import 'package:flutter/material.dart';

class HomeChooseVC extends StatefulWidget {
  HomeChooseVC({Key key}) : super(key: key);

  @override
  _HomeChooseVCState createState() => _HomeChooseVCState();
}

class _HomeChooseVCState extends State<HomeChooseVC> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: Colors.blue,
        appBar: AppBar(
          backgroundColor: Colors.lightBlue,
          title: Text("选择菜单页面"),
        ),
        body: _rowContainer(),
      ),
    );
  }

  Column _rowContainer() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          height: 80,
          alignment: Alignment.center,
          child: Text("请选择"),
        ),
        Container(
          height: 300,
          alignment: Alignment.center,
          child: Row(
            children: [
              Container(
                  width: 150,
                  height: 200,
                  color: Colors.red,
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Image.network(
                          'http://tiebapic.baidu.com/forum/w%3D580/sign=a96ca741eafaaf5184e381b7bc5594ed/7ea6a61ea8d3fd1f2643ad5d274e251f95ca5f38.jpg'),
                      Text(
                        "打包带走",
                        style: TextStyle(color: Colors.yellow),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  )),
              SizedBox(width: 20),
              Container(
                  width: 150,
                  height: 200,
                  color: Colors.red,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Image.network(
                          'http://tiebapic.baidu.com/forum/w%3D580/sign=a96ca741eafaaf5184e381b7bc5594ed/7ea6a61ea8d3fd1f2643ad5d274e251f95ca5f38.jpg'),
                      Text("店内就餐"),
                    ],
                  ))
            ],
          ),
        ),
        Container(
          height: 100,
          color: Colors.green,
          alignment: Alignment.center,
          child: Image.network(
              'http://tiebapic.baidu.com/forum/w%3D580/sign=a96ca741eafaaf5184e381b7bc5594ed/7ea6a61ea8d3fd1f2643ad5d274e251f95ca5f38.jpg'),
        ),
        RaisedButton(
          onPressed: () {
            Navigator.pushNamed(context, "ording");
          },
          child: Container(
            height: 30,
            alignment: Alignment.center,
            child: Text(
              "扫码进入小程序点餐",
              style: TextStyle(color: Colors.white),
            ),
          ),
        )
      ],
    );
  }
}
