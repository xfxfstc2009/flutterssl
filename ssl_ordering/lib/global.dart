/*
 * 页面
*/

import 'package:ssl_ordering/Home/home_choose.dart';
import 'package:flutter/material.dart';

class GlobalManager {
  // 路由总表
  Map<String, WidgetBuilder> _routeMap = {};
  // 拦截参数，用来拦截路由表，进行不同操作
  final _isLogin = true;
  final _otherJudge = true;

  GlobalManager() {
    // 1. 路由进行选择控制器
    _routeMap.addAll(mapForChooseVC());
  }

  // 1.选择内容
  Map<String, WidgetBuilder> mapForChooseVC() {
    return {
      "/": (BuildContext context) => HomeChooseVC(),
    };
  }

  // 2. 登录路由

  // 拦截其他情况路由
  MaterialPageRoute ohterRoute(RouteSettings setting) {
    // 这里可以替换为自定义的 Login 页面
    return MaterialPageRoute(builder: (context) => Scaffold());
  }
}
