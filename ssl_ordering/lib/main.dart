import 'package:flutter/material.dart';
import 'package:ssl_ordering/global.dart';
import 'package:ssl_ordering/Login/login_info.dart'; // 登录页面
import 'package:ssl_ordering/Home/home_main.dart'; // 主页
import 'package:flutter_easyloading/flutter_easyloading.dart'; // Loading

void main() {
  runApp(MyApp());
}

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
// initialRoute: '/',
//       theme: ThemeData(

//           ),
//       home: LoginViewController(),

//     );
//   }
// }

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '打包间',
      initialRoute: '/',

      theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          platform: TargetPlatform.iOS //添加这个属性即可
          ),
      home: LoginViewController(),
      routes: {
        "homeViewController": (BuildContext context) =>
            new HomeMainViewController(),
      },
      // builder: EasyLoading.init(),
    );
  }
}
