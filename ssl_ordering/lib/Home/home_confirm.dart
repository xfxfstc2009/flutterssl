import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class HomeConfirmVC extends StatefulWidget {
  HomeConfirmVC({Key key}) : super(key: key);

  @override
  _HomeConfirmVCState createState() => _HomeConfirmVCState();
}

class _HomeConfirmVCState extends State<HomeConfirmVC> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.lightBlue,
        title: Text("确认订单页面"),
      ),
      body: _bodyContainer(),
    ));
  }

  Container _bodyContainer() {
    return Container(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
            flex: 20,
            child: Container(
              color: Colors.white,
              child: _ordersColumn(),
            )),
        Expanded(
            flex: 3,
            child: Container(
              color: Colors.red,
              child: _bottomButtonRow(),
            )),
      ],
    ));
  }

  // 订单的卡片
  Column _ordersColumn() {
    return Column(
      children: [
        // 1. 店铺
        Expanded(
            flex: 2,
            child: Row(
              children: [
                SizedBox(
                  width: 20,
                ),
                Container(
                  child: Image.network(
                      "https://www.itying.com/images/flutter/1.png"),
                ),
                SizedBox(
                  width: 10,
                ),
                Container(
                  child: Text("十四郎滨江店"),
                )
              ],
            )),
        // 2. 订单
        Expanded(flex: 18, child: _ordersInfoContainer()),
      ],
    );
  }

  // 订单row
  Container _ordersInfoContainer() {
    return Container(
        child: ListView(
      children: [
        Card(
          color: Colors.white,
          elevation: 15.0, //设置阴影
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(14.0))), //设置圆角
          child: Column(
            children: [
              new ListTile(
                title: new Text('已选商品',
                    style: new TextStyle(fontWeight: FontWeight.w300)),
              ),
              new Divider(),
              _infoCell(),
              _infoCell(),
              _infoCell(),
              _infoCell(),
              _infoCell(),
              _infoCell(),
              _infoCell(),
              _infoCell(),
              new Divider(),
              orderCountCell(),
            ],
          ),
        ),
        SizedBox(
          height: 40,
        ),
        Card(
            color: Colors.white,
            elevation: 15.0, //设置阴影
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(14.0))), //设置圆角
            child: Column(
              children: [
                new ListTile(
                  title: new Text('备注',
                      style: new TextStyle(fontWeight: FontWeight.w300)),
                ),
                new Divider(),
                Container(
                  child: Wrap(
                    children: [for (var item in tags) TagItem(item)],
                  ),
                )
              ],
            ))
      ],
    ));
  }

// 单条cell
  Container _infoCell() {
    return Container(
      height: 100,
      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Row(
        children: [
          // 1. 边距
          SizedBox(
            width: 20,
          ),
          // 2. 照片
          Expanded(
              flex: 2,
              child: Container(
                  child: Image.network(
                      "https://www.itying.com/images/flutter/1.png"))),
          Expanded(
              flex: 20,
              child: Column(
                children: [
                  // 1. 第一行商品名+价格
                  Row(
                    children: [
                      Expanded(
                          flex: 10,
                          child: Text("咔滋脆鸡排饭",
                              style: TextStyle(fontWeight: FontWeight.w800))),
                      Expanded(
                          flex: 5,
                          child: Text("X 2",
                              style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black))),
                      Expanded(
                          flex: 5,
                          child: Text("43.3",
                              style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  color: Colors.red))),
                    ],
                  ),
                  Container(
                    color: Colors.yellow,
                    child: Text("12313212"),
                    alignment: Alignment.centerLeft,
                  ),
                  Container(
                    child: Text("12313212"),
                    alignment: Alignment.centerLeft,
                  ),
                  Container(
                    child: Text("12313212"),
                    alignment: Alignment.centerLeft,
                  ),
                  Container(
                    child: Text("12313212"),
                    alignment: Alignment.centerLeft,
                  )
                ],
              ))
        ],
      ),
    );
  }

  // 订单详细
  Column orderCountCell() {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
                child: Text(
              "（共计 3 件 菜品）",
              style: TextStyle(color: Colors.red),
            )),
            Expanded(child: Text("（共计 3 件 菜品）")),
            Expanded(child: Text("包装费")),
          ],
        ),
        SizedBox(
          height: 40,
        ),
        Container(
          child: Text(
            "￥222.0",
            style: TextStyle(color: Colors.red, fontSize: 23.0),
          ),
        )
      ],
    );
  }

  // 底部的按钮条
  Row _bottomButtonRow() {
    return Row(
      children: [
        Expanded(
            child: FlatButton(
          textColor: Colors.white,
          color: Colors.red,
          height: 100,
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text("取消订单"),
        )),
        Expanded(
            child: FlatButton(
          textColor: Colors.white,
          color: Colors.red,
          height: 100,
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text("继续点单"),
        )),
        Expanded(
            child: FlatButton(
          textColor: Colors.white,
          color: Colors.red,
          height: 100,
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text("去支付 >"),
        )),
      ],
    );
  }
}

class TagItem extends StatelessWidget {
  final text;

  TagItem(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(8, 2, 8, 2),
      child: OutlineButton(
        child: Text(text["title"]),
        onPressed: () {
          print("123");
        },
      ),
    );
  }
}

List tags = [
  {
    "title": "卡小哥哥你的东西掉了",
    "check": "0",
  },
  {
    "title": "卡小哥哥你的东西掉了",
    "check": "0",
  },
  {
    "title": "卡小哥哥你的东西掉了",
    "check": "0",
  },
  {
    "title": "卡小哥哥你的东西掉了",
    "check": "0",
  },
  {
    "title": "卡小哥哥你的东西掉了",
    "check": "0",
  },
  {
    "title": "卡小哥哥你的东西掉了",
    "check": "0",
  }
];
