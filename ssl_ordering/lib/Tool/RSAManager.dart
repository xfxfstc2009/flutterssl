import 'package:encrypt/encrypt.dart';
import 'package:pointycastle/asymmetric/api.dart';
import 'dart:async';
import 'package:flutter/services.dart' show rootBundle;

final parser = RSAKeyParser();

class RSAManager {
  static Future<String> encode(String src) async {
    String publicKeyString =
        await rootBundle.loadString('assets/keys/public_key.pem');
    RSAPublicKey publicKey = parser.parse(publicKeyString);

    final encrypter = Encrypter(RSA(publicKey: publicKey));

    String pubStr = encrypter.encrypt(src).base64;
    return pubStr;
  }

  // static Future<String> decode(String decoded) async {
  //   String privateKeyString =
  //       await rootBundle.loadString('keys/private_key.pem');

  //   final privateKey = parser.parse(privateKeyString);

  //   final encrypter = Encrypter(RSA(privateKey: privateKey));
  //   return encrypter.decrypt(Encrypted.fromBase64(decoded));
  // }
}
